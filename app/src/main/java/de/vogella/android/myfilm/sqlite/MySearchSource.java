package de.vogella.android.myfilm.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import de.vogella.android.myfilm.MainActivity;
import de.vogella.android.myfilm.model.Search;

public class MySearchSource {
    private SQLiteDatabase sqLiteDatabase;
    private SQLiteOpenHelper sqLiteOpenHelper;

    public MySearchSource(SQLiteOpenHelper sqLiteOpenHelper){
        this.sqLiteOpenHelper = sqLiteOpenHelper;
    }

    public void openRead(){
        sqLiteDatabase = sqLiteOpenHelper.getReadableDatabase();
    }

    public List<Search> searchFilms(String title){

        List<Search> list = new ArrayList<>();

        this.openRead();
        String selection = MySearchContract.SearchEntry.COLUMN_NAME_TITLE + " like ?";
        String selectionArgs [] = {'%' + title + "%"};
        String projection [] = {
                MySearchContract.SearchEntry.COLUMN_NAME_TITLE,
                MySearchContract.SearchEntry.COLUMN_NAME_TYPE,
                MySearchContract.SearchEntry.COLUMN_NAME_YEAR,
                MySearchContract.SearchEntry.COLUMN_NAME_POSTER
        };

        Cursor cursor = sqLiteDatabase.query(MySearchContract.SearchEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, null);
        while(cursor.moveToNext()){
            String realTitle = cursor.getString(cursor.getColumnIndexOrThrow(MySearchContract.SearchEntry.COLUMN_NAME_TITLE));
            Log.i("CCC", realTitle);
            String year = cursor.getString(cursor.getColumnIndexOrThrow(MySearchContract.SearchEntry.COLUMN_NAME_YEAR));
            String type = cursor.getString(cursor.getColumnIndexOrThrow(MySearchContract.SearchEntry.COLUMN_NAME_TYPE));
            String poster = cursor.getString(cursor.getColumnIndexOrThrow(MySearchContract.SearchEntry.COLUMN_NAME_POSTER));
            String imdbId = cursor.getString(1);

            Search search = new Search(realTitle, year, imdbId, type, poster);
            list.add(search);
        }

        return list;
    }


}
