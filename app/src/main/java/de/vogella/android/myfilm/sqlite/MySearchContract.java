package de.vogella.android.myfilm.sqlite;

import de.vogella.android.myfilm.recyclerView.MyAdapterSearch;

public class MySearchContract {
    private MySearchContract(){}
    public static String SQL_CREATE_TABLE_SEARCH =
            "CREATE TABLE " + SearchEntry.TABLE_NAME + " (" +
                    SearchEntry.COLUMN_NAME_IMDB_ID + " TEXT PRIMARY KEY, " +
                    SearchEntry.COLUMN_NAME_TITLE + " TEXT , " +
                    SearchEntry.COLUMN_NAME_YEAR + " INTEGER, " +
                    SearchEntry.COLUMN_NAME_TYPE + " TEXT, " +
                    SearchEntry.COLUMN_NAME_POSTER + " TEXT)";

    public static String SQL_DELETE_TABLE_SEARCH =
            "DROP TABLE IF EXISTS " + SearchEntry.TABLE_NAME;

    public static class SearchEntry {
        public static final String TABLE_NAME ="search";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_YEAR = "year";
        public static final String COLUMN_NAME_IMDB_ID = "imdbId";
        public static final String COLUMN_NAME_TYPE = "type";
        public static final String COLUMN_NAME_POSTER = "poster";
    }
}
