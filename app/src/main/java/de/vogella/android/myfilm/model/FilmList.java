package de.vogella.android.myfilm.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FilmList {
    @SerializedName("data")
    public List<Film> films;
}
