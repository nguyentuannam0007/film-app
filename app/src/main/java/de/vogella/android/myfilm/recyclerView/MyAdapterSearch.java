package de.vogella.android.myfilm.recyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import de.vogella.android.myfilm.R;
import de.vogella.android.myfilm.model.Search;
import de.vogella.android.myfilm.recyclerView.ItemClickListener;

public class MyAdapterSearch extends RecyclerView.Adapter<MyAdapterSearch.ViewHolder> {
    public static final int VIEW_TYPE_LOAD = 0;
    public static final int VIEW_TYPE_ITEM = 1;
    List<Search> searches;
    Context context;
    private ItemClickListener itemClickListener;

    public MyAdapterSearch(Context context, List<Search> searchList, ItemClickListener itemClickListener){
        this.context = context;
        this.searches = searchList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public MyAdapterSearch.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_search, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapterSearch.ViewHolder holder, final int position) {
        final Search search = searches.get(position);
        holder.txtType.setText("Type: " + search.getType());
        holder.txtTitle.setText(search.getTitle());
        holder.txtYear.setText("Year: " + search.getYear());
        Picasso.get().load(search.getPoster()).into(holder.imgPoster);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (itemClickListener != null)
                    itemClickListener.onClick(view,search,false);
            }
        });
    }

    @Override
    public int getItemCount() {
        return searches.size();
    }

    @Override
    public int getItemViewType(int position) {
        return searches.get(position) == null ? VIEW_TYPE_LOAD : VIEW_TYPE_ITEM;
    }

    public class ViewHolder extends RecyclerView.ViewHolder  {
        public TextView txtTitle, txtType, txtYear;
        public ImageView imgPoster;

        public ViewHolder(View v){
            super(v);
            txtTitle = (TextView) v.findViewById(R.id.txtTitle);
            txtType = (TextView) v.findViewById(R.id.txtType);
            txtYear = (TextView) v.findViewById(R.id.txtYear);
            imgPoster = (ImageView) v.findViewById(R.id.imgPoster);
        }
    }
}