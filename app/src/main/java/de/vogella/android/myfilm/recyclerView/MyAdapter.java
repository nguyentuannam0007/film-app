package de.vogella.android.myfilm.recyclerView;

import android.content.Context;
import android.text.Layout;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.List;

import de.vogella.android.myfilm.R;
import de.vogella.android.myfilm.model.Film;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    public static final int VIEW_TYPE_LOAD = 0;
    public static final int VIEW_TYPE_ITEM = 1;
    List<Film> filmList;
    Context context;
    public MyAdapter(Context context, List<Film> filmList){
        this.context = context;
        this.filmList = filmList;
    }

    @NonNull
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.ViewHolder holder, int position) {
        Film film = filmList.get(position);
        holder.txtImdb.setText("Imdb: " + film.getImdbRating());
        holder.txtContent.setText(film.getPlot());
        holder.txtTitle.setText(film.getTitle());

    }

    @Override
    public int getItemCount() {
        return filmList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return filmList.get(position) == null ? VIEW_TYPE_LOAD : VIEW_TYPE_ITEM;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView txtTitle, txtContent, txtImdb;
        public ViewHolder(View v){
            super(v);
            txtTitle = (TextView) v.findViewById(R.id.titleFilm);
            txtContent = (TextView) v.findViewById(R.id.contentFilm);
            txtImdb = (TextView) v.findViewById(R.id.imdbFilm);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
    }
}
