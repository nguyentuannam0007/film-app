package de.vogella.android.myfilm.retrofit;

import java.util.List;

import de.vogella.android.myfilm.model.Film;
import de.vogella.android.myfilm.model.FilmList;
import de.vogella.android.myfilm.model.Searches;
import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {
    @GET("?")
    Observable<Film> getFilmByTitle(@Query("apikey") String apiKey, @Query("t") String title);

    @GET("?")
    Observable<Film> getFilmById(@Query("apikey") String apiKey, @Query("i") String imdbID);


    @GET("?")
    Observable<Searches> searchFilms(@Query("apikey") String apiKey, @Query("s") String title);
}
