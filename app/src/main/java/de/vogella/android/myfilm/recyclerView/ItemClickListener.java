package de.vogella.android.myfilm.recyclerView;

import android.view.View;

import de.vogella.android.myfilm.model.Film;
import de.vogella.android.myfilm.model.Search;

public interface ItemClickListener {
    void onClick(View view, Search itemSeach, boolean isLongClick);
}
