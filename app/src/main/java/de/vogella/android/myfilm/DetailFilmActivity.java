package de.vogella.android.myfilm;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import de.vogella.android.myfilm.model.Film;
import de.vogella.android.myfilm.retrofit.ApiService;
import de.vogella.android.myfilm.retrofit.RetrofitClient;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class DetailFilmActivity extends AppCompatActivity {

    private TextView txtTitle, txtYear, txtImdb, txtContent, infoSearch;
    private ImageView imgPoster;

    private ApiService apiService;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private String imdbID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_film);
        mapping();
        getData();

        // Create API
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null){
            Retrofit retrofitClient = RetrofitClient.getClient(this);
            apiService = retrofitClient.create(ApiService.class);
            getDataFromAPI();
        }else{
            alert("Missing Internet");
        }

    }

    private void alert(String message) {
        txtTitle.setText("");
        txtYear.setText("");
        txtImdb.setText("");
        txtContent.setText("");
        imgPoster.setImageBitmap(null);
        infoSearch.setText(message);
    }

    private void mapping(){
        txtTitle = (TextView) findViewById(R.id.titleDetail);
        txtYear = (TextView) findViewById(R.id.yearDetail);
        txtImdb = (TextView) findViewById(R.id.imdbDetail);
        txtContent = (TextView) findViewById(R.id.contentDetail);
        imgPoster = (ImageView) findViewById(R.id.posterDetail);
        infoSearch = (TextView) findViewById(R.id.infoSearch);

    }

    private void getData(){
        // getData
        Intent intent = getIntent();
        imdbID = intent.getStringExtra("imdbID");
    }

    private void getDataFromAPI() {
        // getData from Internet
        Observable<Film> observable = apiService.getFilmById(RetrofitClient.API_KEY, imdbID);
        compositeDisposable.add(observable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Consumer<Film>() {
                @Override
                public void accept(Film film) throws Exception {
                    if(film != null){
                        Log.i("AAA", "Enter here");
                        displayFilm(film);
                    }else{
                        Log.i("EEE", "Cannot get Film");
                    }
                }
            }));
    }

    private void displayFilm(Film film){
        infoSearch.setText("");
        Log.i("TTT", film.getTitle());
        txtTitle.setText(film.getTitle());
        txtYear.setText("Year: " + film.getYear());
        txtImdb.setText("Imdb: " + film.getImdbRating());
        txtContent.setText(film.getPlot());
        Picasso.get().load(film.getPoster()).into(imgPoster);
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }
}
