package de.vogella.android.myfilm.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MySearchDbHelper extends SQLiteOpenHelper {
    public static final int DATA_VERSION = 1;
    public static final String DATABASE_NAME = "Films.db";

    public MySearchDbHelper(Context context){
        super(context, DATABASE_NAME, null, DATA_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(MySearchContract.SQL_CREATE_TABLE_SEARCH);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(MySearchContract.SQL_DELETE_TABLE_SEARCH);
        onCreate(db);
    }
}
