package de.vogella.android.myfilm;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.RecyclerView;

import android.app.SearchManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;

import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import de.vogella.android.myfilm.model.Film;
import de.vogella.android.myfilm.model.FilmList;
import de.vogella.android.myfilm.model.Search;
import de.vogella.android.myfilm.model.Searches;
import de.vogella.android.myfilm.recyclerView.ItemClickListener;
import de.vogella.android.myfilm.recyclerView.MyAdapter;
import de.vogella.android.myfilm.recyclerView.MyAdapterSearch;
import de.vogella.android.myfilm.retrofit.ApiService;
import de.vogella.android.myfilm.retrofit.PrefUtils;
import de.vogella.android.myfilm.retrofit.RetrofitClient;
import de.vogella.android.myfilm.sqlite.MySearchContract;
import de.vogella.android.myfilm.sqlite.MySearchDbHelper;
import de.vogella.android.myfilm.sqlite.MySearchSource;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity implements ItemClickListener {

    private RecyclerView recyclerView;
    private SearchView searchView;
    private TextView infoSeach;
    // Retrofit and RxJava
    private ApiService apiService;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    // SQlite
    MySearchDbHelper mySearchDbHelper = new MySearchDbHelper(this);

    List<Film> filmList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Init view
        infoSeach = (TextView) findViewById(R.id.info);

        // Init APIService
        Retrofit retrofit = RetrofitClient.getClient(this);
        apiService = retrofit.create(ApiService.class);

        // Init RecyclerView
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        getDataFromSQLDatabase();

    }

    public void displayData(Film film){
        filmList.add(film);
        MyAdapter myAdapter = new MyAdapter(this, filmList);
        recyclerView.setAdapter(myAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);

        final SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.i("BBB", "Begin download information film");

                ConnectivityManager conManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conManager.getActiveNetworkInfo();
                if (netInfo != null){
                    Log.i("MMM","Search on Internet");
                    searchFilm(query);
                }else{
                    Log.i("MMM", "Search on database");
                    MySearchSource mySearchSource = new MySearchSource(mySearchDbHelper);
                    List<Search> listSearch = mySearchSource.searchFilms(query);
                    updateViewFilm(listSearch);
                }
                return true;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return true;
    }

    private void searchFilm(String query) {
        Observable<Searches> searchesObservable = apiService.searchFilms(RetrofitClient.API_KEY, query);
        compositeDisposable.add(searchesObservable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()
            ).subscribe(new Consumer<Searches>() {
            @Override
            public void accept(Searches searches) throws Exception {
                if(Integer.valueOf(searches.getTotalResults()) != 0){
                    updateViewFilm(searches.getSearch());
                    storeData(searches);
                }else{
                    Toast.makeText(MainActivity.this, "Not found film !", Toast.LENGTH_SHORT).show();
                }
            }
        }));
    }

    private void getDataFromSQLDatabase(){
        SQLiteDatabase db = mySearchDbHelper.getReadableDatabase();
        String [] projections = {
                MySearchContract.SearchEntry.COLUMN_NAME_IMDB_ID,
                MySearchContract.SearchEntry.COLUMN_NAME_TITLE,
                MySearchContract.SearchEntry.COLUMN_NAME_YEAR,
                MySearchContract.SearchEntry.COLUMN_NAME_TYPE,
                MySearchContract.SearchEntry.COLUMN_NAME_POSTER,
        };

        String sortOrder = MySearchContract.SearchEntry.COLUMN_NAME_YEAR + " DESC";

        Cursor cursor = db.query(
                MySearchContract.SearchEntry.TABLE_NAME,
                projections,
                null,
                null,
                null,
                null,
                sortOrder);
        List<Search> films = new ArrayList<>();
        while(cursor.moveToNext()){
            String title = cursor.getString(cursor.getColumnIndexOrThrow(MySearchContract.SearchEntry.COLUMN_NAME_TITLE));
            String year = cursor.getString(cursor.getColumnIndexOrThrow(MySearchContract.SearchEntry.COLUMN_NAME_YEAR));
            String type = cursor.getString(cursor.getColumnIndexOrThrow(MySearchContract.SearchEntry.COLUMN_NAME_TYPE));
            String poster = cursor.getString(cursor.getColumnIndexOrThrow(MySearchContract.SearchEntry.COLUMN_NAME_POSTER));
            String imdbId = cursor.getString(cursor.getColumnIndexOrThrow(MySearchContract.SearchEntry.COLUMN_NAME_IMDB_ID));

            Search search = new Search(title, year, imdbId, type, poster);
            films.add(search);
        }

        updateViewFilm(films);

    }

    private void storeData(Searches searches){
        SQLiteDatabase db = mySearchDbHelper.getWritableDatabase();
        for (Search search : searches.getSearch()){
            ContentValues values = new ContentValues();
            values.put(MySearchContract.SearchEntry.COLUMN_NAME_IMDB_ID, search.getImdbID());
            values.put(MySearchContract.SearchEntry.COLUMN_NAME_TITLE, search.getTitle());
            values.put(MySearchContract.SearchEntry.COLUMN_NAME_YEAR, search.getYear());
            values.put(MySearchContract.SearchEntry.COLUMN_NAME_TYPE, search.getType());
            values.put(MySearchContract.SearchEntry.COLUMN_NAME_POSTER, search.getPoster());


            long newRowId = db.insert(MySearchContract.SearchEntry.TABLE_NAME, null, values);
            if(newRowId > 0){
                Log.i("SSS", "Insert data ok !");
            }else{
                Log.i("EEE", "Cannot insert data");
            }
        }
    }

    private void updateViewFilm(List<Search> searchList){
        infoSeach.setText("");
        Log.i("ZZZ", String.valueOf(searchList.size()));
        if(searchList.size() > 0){
            MyAdapterSearch myAdapterSearch = new MyAdapterSearch(this, searchList, this);
            recyclerView.removeAllViews();
            recyclerView.setAdapter(myAdapterSearch);
        }else{
            recyclerView.setAdapter(null);
            recyclerView.removeAllViewsInLayout();
            recyclerView.removeAllViews();
            infoSeach.setText("Not found your film.");
        }

    }

    @Override
    protected void onStop() {
        compositeDisposable.dispose();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }

    @Override
    public void onClick(View view, Search itemSearch, boolean isLongClick) {
        Intent intent = new Intent(this, DetailFilmActivity.class);
        intent.putExtra("imdbID", itemSearch.getImdbID());
        startActivity(intent);

    }
}
